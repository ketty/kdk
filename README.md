# kdk

## Getting started

- execute `setup.sh`
- create a config file in `config/local.js` as specified [in the docs](https://docs.ketty.community/docs/deploy/Deploy%20Ketty%20in%20production#config-file)
- if you want to develop for ketida run `docker compose up`
- if you want to develop for oen run `docker compose -f docker-compose.oen.yml up`
- if you want to develop for vanilla run `docker compose -f docker-compose.vanilla.yml up`
